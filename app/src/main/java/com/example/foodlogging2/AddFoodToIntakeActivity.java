package com.example.foodlogging2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;




import java.text.SimpleDateFormat;

import java.util.Date;


public class AddFoodToIntakeActivity extends AppCompatActivity {


    EditText foodIntake;
    TextView foodIntakeID;
    TextView foodIntakePortionSize;
    TextView foodIntakeCalories;
    TextView foodIntakeFat;
    TextView foodIntakeProtein;
    TextView foodIntakeCarbohydrates;
    TextView foodIntakeFiber;
    TextView foodIntakeOmega3;
    TextView foodIntakeOmega6;

    Button submitFoodIntake;
    Button cancelFoodIntake;
    RadioGroup radioGroup;
    RadioButton radioButton;

    //Current date
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date GetDate = new Date();
    String currentDate = timeStampFormat.format(GetDate);

    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;

    private static final String TAG = "FoodActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food_to_intake);

        foodIntake = findViewById(R.id.etFoodIntake);
        foodIntakeID = findViewById(R.id.tvFoodIntakeID);
        foodIntakePortionSize = findViewById(R.id.tvFoodIntakePortionSize);
        foodIntakeCalories = findViewById(R.id.tvFoodIntakeCalories);
        foodIntakeFat = findViewById(R.id.tvFoodIntakeFat);
        foodIntakeProtein = findViewById(R.id.tvFoodIntakeProtein);
        foodIntakeCarbohydrates = findViewById(R.id.tvFoodIntakeCarbohydrates);
        foodIntakeFiber = findViewById(R.id.tvFoodIntakeFiber);
        foodIntakeOmega3 = findViewById(R.id.tvFoodIntakeOmega3);
        foodIntakeOmega6 = findViewById(R.id.tvFoodIntakeOmega6);

        submitFoodIntake = findViewById(R.id.btnSubmitFoodToIntake);
        cancelFoodIntake = findViewById(R.id.btnCancelAddFood);

        radioGroup = findViewById(R.id.radioGroup);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();




        if (getIntent().hasExtra("selected_food")){
            final Food food = getIntent().getParcelableExtra("selected_food");

                foodIntakeID.setText(food.foodID);
                foodIntakePortionSize.setText( "Veličina porcije   " + food.portionSize + " g");
                foodIntakeCalories.setText("Kalorije   " +food.energy + " kcal");
                foodIntakeFat.setText("Masti   " +food.fat + " g");
                foodIntakeProtein.setText("Proteini   " + food.protein + " g");
                foodIntakeCarbohydrates.setText("Ugljikohidrati   " + food.carbohydrate + " g");
                foodIntakeFiber.setText("Vlakna  " + food.fiber + " g");
                foodIntakeOmega3.setText("Omega3  "+ food.omega3 + " g");
                foodIntakeOmega6.setText("Omega6  "+food.omega6 + " g");

                submitFoodIntake.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String user_id = firebaseAuth.getCurrentUser().getUid();

                        DatabaseReference dailyFoodIntake = firebaseDatabase.getReference("Users").child(user_id);

                        String intakePortionSize = foodIntake.getText().toString();
                        Double intakePortionSizeNumberInGrams = Double.parseDouble(intakePortionSize);


                        Double portionSizeNumber = Double.parseDouble(food.portionSize);
                        Double portionCaloriesNumber = Double.parseDouble(food.energy);
                        Double portionFatNumber = Double.parseDouble(food.fat);
                        Double portionProteinNumber = Double.parseDouble(food.protein);
                        Double portionCarbohydratesNumber = Double.parseDouble(food.carbohydrate);
                        Double portionFibersNumber = Double.parseDouble(food.fiber);
                        Double portionOmega3sNumber = Double.parseDouble(food.omega3);
                        Double portionOmega6sNumber = Double.parseDouble(food.omega6);

                        Double intakePortionSizeNumber = intakePortionSizeNumberInGrams/portionSizeNumber;

                        Double intakePortionSizeNumberCalculated = portionSizeNumber * intakePortionSizeNumber;
                        Double intakePortionCaloriesNumber = portionCaloriesNumber * intakePortionSizeNumber;
                        Double intakePortionFatNumber = portionFatNumber * intakePortionSizeNumber;
                        Double intakePortionProteinNumber = portionProteinNumber * intakePortionSizeNumber;
                        Double intakePortionCarbohydratesNumber = portionCarbohydratesNumber * intakePortionSizeNumber;
                        Double intakePortionFiberNumber = portionFibersNumber * intakePortionSizeNumber;
                        Double intakePortionOmega3Number = portionOmega3sNumber * intakePortionSizeNumber;
                        Double intakePortionOmega6Number = portionOmega6sNumber * intakePortionSizeNumber;


                        Double intakePortionSizeNumberCalculatedRounded = ProfileActivity.round(intakePortionSizeNumberCalculated,2);
                        Double intakePortionCaloriesNumberRounded = ProfileActivity.round(intakePortionCaloriesNumber, 2);
                        Double intakePortionFatNumberRounded = ProfileActivity.round(intakePortionFatNumber,2 );
                        Double intakePortionProteinNumberRounded = ProfileActivity.round(intakePortionProteinNumber, 2);
                        Double intakePortionCarbohydratesNumberRounded = ProfileActivity.round(intakePortionCarbohydratesNumber, 2);
                        Double intakePortionFibersNumberRounded = ProfileActivity.round(intakePortionFiberNumber, 2);
                        Double intakePortionOmega3sNumberRounded = ProfileActivity.round(intakePortionOmega3Number, 2);
                        Double intakePortionOmega6sNumberRounded = ProfileActivity.round(intakePortionOmega6Number,2);

                        String intakePortionSizeMultiplied = intakePortionSizeNumberCalculatedRounded.toString();
                        String intakeCaloriesMultiplied = intakePortionCaloriesNumberRounded.toString();
                        String intakeFatMultiplied = intakePortionFatNumberRounded.toString();
                        String intakeProteinMultiplied = intakePortionProteinNumberRounded.toString();
                        String intakeCarbohydratesMultiplied = intakePortionCarbohydratesNumberRounded.toString();
                        String intakeFibersMultiplied = intakePortionFibersNumberRounded.toString();
                        String intakeOmega3sMultiplied = intakePortionOmega3sNumberRounded.toString();
                        String intakeOmega6sMultiplied = intakePortionOmega6sNumberRounded.toString();
                        String intakeFoodId = food.foodID;

                        Food foodInformationDaily = new Food (intakeFoodId,intakePortionSizeMultiplied,intakeCaloriesMultiplied, intakeProteinMultiplied,intakeFatMultiplied , intakeCarbohydratesMultiplied,
                                intakeFibersMultiplied, intakeOmega3sMultiplied, intakeOmega6sMultiplied);

                        int radioId = radioGroup.getCheckedRadioButtonId();
                        radioButton = findViewById(radioId);

                        String selectedMeal = radioButton.getText().toString();

                        dailyFoodIntake.child(currentDate).child(selectedMeal).child(intakeFoodId).setValue(foodInformationDaily).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()){
                                    Toast.makeText(AddFoodToIntakeActivity.this, "New Food Created", Toast.LENGTH_LONG).show();

                                    startActivity(new Intent(AddFoodToIntakeActivity.this, ProfileActivity.class));

                                }else {

                                    Toast.makeText(AddFoodToIntakeActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                });


                cancelFoodIntake.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(AddFoodToIntakeActivity.this, ProfileActivity.class));
                    }
                });

            Log.d(TAG, "onCreate "+ food.toString());

        }
    }

    public void checkButton (View v){
        int radioId = radioGroup.getCheckedRadioButtonId();

        radioButton = findViewById(radioId);
    }
}


