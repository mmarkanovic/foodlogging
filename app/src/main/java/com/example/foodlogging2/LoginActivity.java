package com.example.foodlogging2;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;


public class LoginActivity extends AppCompatActivity {

    EditText userEmail;
    EditText userPass;
    Button userLogin;
    TextView textView;
    TextView textViewForgotPassword;
    TextInputLayout textLoginEmail;
    TextInputLayout textLoginPassword;

    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        userEmail = findViewById(R.id.etUserEmail);
        userPass = findViewById(R.id.etUserPass);
        userLogin = findViewById(R.id.btnUserLogin);
        textView = findViewById(R.id.linkSignup);
        textViewForgotPassword = findViewById(R.id.tvForgotPassword);

        textLoginEmail = findViewById(R.id.text_login_email);
        textLoginPassword = findViewById(R.id.text_login_password);

        firebaseAuth = FirebaseAuth.getInstance();


       if (firebaseAuth.getCurrentUser() != null && firebaseAuth.getCurrentUser().isEmailVerified()){
           startActivity(new Intent(LoginActivity.this, ProfileActivity.class));
       }




       userLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!validateEmail() | !validate(textLoginPassword)){
                    return;
                }

                firebaseAuth.signInWithEmailAndPassword(userEmail.getText().toString(), userPass.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            if (firebaseAuth.getCurrentUser().isEmailVerified()){
                                startActivity(new Intent(LoginActivity.this, ProfileActivity.class));
                            }else{
                                Toast.makeText(LoginActivity.this, "Molimo da verificirate svoju email adresu", Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(LoginActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

       textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            }
        });

       textViewForgotPassword.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
           }
       });

    }

    private boolean validate (TextInputLayout inputLayout){
        String input = Objects.requireNonNull(inputLayout.getEditText()).getText().toString().trim();

        if(input.isEmpty()){
            inputLayout.setError("Polje ne može biti prazno");
            return false;
        }else{
            inputLayout.setError(null);
            return true;
        }
    }

    private boolean validateEmail(){
        String emailInput = Objects.requireNonNull(textLoginEmail.getEditText()).getText().toString().trim();

        if (emailInput.isEmpty()){
            textLoginEmail.setError("Polje ne može biti prazno");
            return false;
        }else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()){
            textLoginEmail.setError("Unesite ispravnu email adresu");
            return false;
        }else{
            textLoginEmail.setError(null);
            return true;
        }
    }
}
