package com.example.foodlogging2;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Food implements Serializable, Parcelable {

    protected Food(Parcel in) {
        foodID = in.readString();
        portionSize = in.readString();
        energy = in.readString();
        protein = in.readString();
        fat = in.readString();
        carbohydrate = in.readString();
        fiber = in.readString();
        omega3 = in.readString();
        omega6 = in.readString();
    }

    public static final Creator<Food> CREATOR = new Creator<Food>() {
        @Override
        public Food createFromParcel(Parcel in) {
            return new Food(in);
        }

        @Override
        public Food[] newArray(int size) {
            return new Food[size];
        }
    };

    /*
    public Food(String foodIDs, String intakeCaloriesMultiplied, String intakePortionSizeMultiplied, String intakeFatMultiplied, String intakeProteinMultiplied, String intakeCarbohydratesMultiplied) {
        this.foodID = foodIDs;
        this.energy=intakeCaloriesMultiplied;
        this.portionSize = intakePortionSizeMultiplied;
        this.fat = intakeFatMultiplied;
        this.protein = intakeProteinMultiplied;
        this.protein = intakeCarbohydratesMultiplied;

    }

     */

    public String getFoodID() {
        return foodID;
    }

    public void setFoodID(String foodID) {
        this.foodID = foodID;
    }

    public String getPortionSize() {
        return portionSize;
    }

    public void setPortionSize(String portionSize) {
        this.portionSize = portionSize;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public String getProtein() {
        return protein;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(String carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public String getFiber() {return  fiber;}

    public void setFiber(String fiber) {this.fiber= fiber;}

    public String getOmega3() {return omega3;}

    public void setOmega3 (String omega3) {this.omega3 = omega3;}

    public String getOmega6() {return omega6;}

    public void setOmega6(String omega6) {this.omega6 = omega6;}

    public String foodID;
    public String portionSize;
    public String energy;
    public String protein;
    public String fat;
    public String carbohydrate;
    public String fiber;
    public String omega3;
    public String omega6;

    public Food() {
    }

    public Food(String fats, String omega3s, String omega6s){
        this.fat=fats;
        this.omega3 = omega3s;
        this.omega6 = omega6s;

    }

    public Food (String foodIDs, String portionSizes, String energies, String proteins, String fats, String carbohydrates, String fibers, String omega3s, String omega6s){
        this.foodID = foodIDs;
        this.portionSize = portionSizes;
        this.energy = energies;
        this.protein = proteins;
        this.fat = fats;
        this.carbohydrate = carbohydrates;
        this.fiber = fibers;
        this.omega3 = omega3s;
        this.omega6 = omega6s;

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(foodID);
        dest.writeString(portionSize);
        dest.writeString(energy);
        dest.writeString(protein);
        dest.writeString(fat);
        dest.writeString(carbohydrate);
        dest.writeString(fiber);
        dest.writeString(omega3);
        dest.writeString(omega6);
    }
}
