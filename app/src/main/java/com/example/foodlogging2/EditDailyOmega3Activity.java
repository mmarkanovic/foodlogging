package com.example.foodlogging2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class EditDailyOmega3Activity extends AppCompatActivity {

    EditText fatsNumber;
    TextView textViewFats;
    Button submit, cancel;

    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;

    TextInputLayout textUpdateFats;

    String userID;


    DatabaseReference databaseReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_daily_fat_limit);

        fatsNumber = findViewById(R.id.etNewDailyFats);
        submit = findViewById(R.id.btnSubmitNewDailyFats);
        cancel = findViewById(R.id.btnCancelNewDailyFats);
        textViewFats = findViewById(R.id.tvUpdateFats);

        textUpdateFats = findViewById(R.id.text_addnew_dailyfats);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();

        userID = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users")
                .child(userID).child("userData").child("omega3");

        textViewFats.setText(R.string.omega3UpdateText);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validate(textUpdateFats)){
                    return;
                }

                String newFatsText = fatsNumber.getText().toString();

                databaseReference.setValue(newFatsText).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override

                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(EditDailyOmega3Activity.this, "Updated Successful", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(EditDailyOmega3Activity.this, ProfileActivity.class);

                        startActivity(intent);
                    }
                });
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditDailyOmega3Activity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });


    }
    private boolean validate (TextInputLayout inputLayout){
        String input = Objects.requireNonNull(inputLayout.getEditText()).getText().toString().trim();

        if(input.isEmpty()){
            inputLayout.setError("Polje ne može biti prazno");
            return false;
        }else{
            inputLayout.setError(null);
            return true;
        }
    }
}
