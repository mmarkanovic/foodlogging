package com.example.foodlogging2;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;


public class IntakeRecyclerAdapterBreakfast extends RecyclerView.Adapter<IntakeRecyclerAdapterBreakfast.ViewHolder> {

    Context context;
    ArrayList<Food> foodArrayList;

    public IntakeRecyclerAdapterBreakfast(Context c, ArrayList<Food> foods){

        context = c;
        foodArrayList = foods;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.intake_card_holder, viewGroup , false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.id.setText(foodArrayList.get(position).getFoodID());

        String setTextPortionSize =foodArrayList.get(position).getPortionSize() + " g";
        viewHolder.portionSize.setText(setTextPortionSize);

        String setTextEnergy = foodArrayList.get(position).getEnergy() + " kcal";
        viewHolder.energy.setText(setTextEnergy);

        String setTextFat =foodArrayList.get(position).getFat() + " g";
        viewHolder.fat.setText(setTextFat);

        String setTextOmega3 = foodArrayList.get(position).getOmega3() + " g";
        viewHolder.omega3.setText(setTextOmega3);

        String setTextOmega6 = foodArrayList.get(position).getOmega6() + " g";
        viewHolder.omega6.setText(setTextOmega6);


        viewHolder.edit.setVisibility(View.VISIBLE);
        viewHolder.onClickEdit(position);
        viewHolder.delete.setVisibility(View.VISIBLE);
        viewHolder.onClickDelete(position);
    }


    @Override
    public int getItemCount() {
        return foodArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView id, energy, fat, portionSize, omega3, omega6;
        Button edit, delete;

        public ViewHolder(View itemView){
            super (itemView);

            id = itemView.findViewById(R.id.tvIntakeFoodCardID);
            portionSize = itemView.findViewById(R.id.tvIntakeCardPortionSize);
            energy = itemView.findViewById(R.id.tvIntakeFoodCardEnergy);
            fat = itemView.findViewById(R.id.tvIntakeFoodCardFat);
            omega3 = itemView.findViewById(R.id.tvIntakeFoodCardOmega3);
            omega6 = itemView.findViewById(R.id.tvIntakeFoodCardOmega6);

            edit = itemView.findViewById(R.id.btnEditIntakeFood);
            delete = itemView.findViewById(R.id.btnDeleteIntakeFood);
        }

        public void onClickEdit (final int position){

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AddFoodToIntakeActivity.class);
                    intent.putExtra("selected_food", (Parcelable) foodArrayList.get(position));
                    context.startActivity(intent);

                    Toast.makeText(context, "Item clicked", Toast.LENGTH_LONG ).show();
                }
            });
        }



        public void onClickDelete(final int position){

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProfileActivity.class);
                    intent.putExtra("deleted_food_breakfast", (Parcelable) foodArrayList.get(position));
                    intent.putExtra("food_id_breakfast",foodArrayList.get(position).foodID);
                    context.startActivity(intent);
                }
            });
        }
    }
}