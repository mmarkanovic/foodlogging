package com.example.foodlogging2;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class AllFoodAdapterClass extends RecyclerView.Adapter<AllFoodAdapterClass.RecyclerViewHolder> {

    private ArrayList<Food> foodInformation;
    private OnItemListener itemListener;


    public AllFoodAdapterClass( ArrayList<Food> foodInformation, OnItemListener onItemListener){

        this.foodInformation = foodInformation;
        this.itemListener = onItemListener;
    }


    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_holder, viewGroup, false);

        return new RecyclerViewHolder(view, itemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, int i) {

        recyclerViewHolder.id.setText(foodInformation.get(i).getFoodID());

        String setTextEnergyAllFood = foodInformation.get(i).getEnergy() + " kcal";
        recyclerViewHolder.energy.setText(setTextEnergyAllFood);

        String setTextFatAllFood = foodInformation.get(i).getFat() + " g";
        recyclerViewHolder.fat.setText(setTextFatAllFood);

        String setTextPortionSizeAllFood =foodInformation.get(i).getPortionSize() + " g";
        recyclerViewHolder.portionSize.setText(setTextPortionSizeAllFood);

        String setTextOmega3 = foodInformation.get(i).getOmega3() + " g";
        recyclerViewHolder.omega3.setText(setTextOmega3);

        String setTextOmega6 = foodInformation.get(i).getOmega6() + " g";
        recyclerViewHolder.omega6.setText(setTextOmega6);


    }

    @Override
    public int getItemCount() {
        return foodInformation.size();
    }



    //----------------------------------------------------------------
    public static class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView id, energy, fat, portionSize, omega3, omega6;
        DatabaseReference databaseReference;
        OnItemListener onItemListener;

        public RecyclerViewHolder(@NonNull final View itemView, OnItemListener onItemListener) {
            super(itemView);

            databaseReference = FirebaseDatabase.getInstance().getReference().child("Food");
            id = itemView.findViewById(R.id.tvFoodCardID);
            energy = itemView.findViewById(R.id.tvFoodCardEnergy);
            fat = itemView.findViewById(R.id.tvFoodCardFat);
            portionSize = itemView.findViewById(R.id.tvCardPortionSize);
            omega3 = itemView.findViewById(R.id.tvFoodCardOmega3);
            omega6 = itemView.findViewById(R.id.tvFoodCardOmega6);
            this.onItemListener = onItemListener;

            itemView.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {
            onItemListener.onItemClick(getAdapterPosition());

        }
    }

    public interface OnItemListener {
        void onItemClick(int position);
    }


}
