package com.example.foodlogging2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class AddNewFoodActivity extends AppCompatActivity {

    EditText foodName;
    EditText portionSize;
    EditText energy;
    EditText protein;
    EditText fat;
    EditText carbohydrate;
    EditText fiber;
    EditText omega3;
    EditText omega6;
    Button submit;
    Button cancel;
    TextInputLayout textNewFoodName;
    TextInputLayout textNewFoodPortionSize;
    TextInputLayout textNewFoodEnergy;
    TextInputLayout textNewFoodProteins;
    TextInputLayout textNewFoodFats;
    TextInputLayout textNewFoodCarbohydrates;
    TextInputLayout textNewFoodFibers;
    TextInputLayout textNewFoodOmega3;
    TextInputLayout textNewFoodOmega6;


    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_addnewfood);

        foodName = findViewById(R.id.etFoodName);
        portionSize = findViewById(R.id.etPortionSize);
        energy = findViewById(R.id.etEnergy);
        protein = findViewById(R.id.etProtein);
        fat = findViewById(R.id.etFatsInPortion);
        carbohydrate = findViewById(R.id.etCarbohydrates);
        fiber = findViewById(R.id.etFibersInPortion);
        omega3 = findViewById(R.id.etOmega3InPortion);
        omega6 = findViewById(R.id.etOmega6InPortion);
        submit= findViewById(R.id.btnSubmitFood);
        cancel= findViewById(R.id.btnCancelSubmitFood);

        textNewFoodName = findViewById(R.id.text_addnew_foodname);
        textNewFoodPortionSize = findViewById(R.id.text_addnew_portionsize);
        textNewFoodEnergy = findViewById(R.id.text_addnew_energy);
        textNewFoodProteins = findViewById(R.id.text_addnew_protein);
        textNewFoodFats = findViewById(R.id.text_addnew_fats);
        textNewFoodCarbohydrates = findViewById(R.id.text_addnew_carbohydrates);
        textNewFoodFibers = findViewById(R.id.text_addnew_fibers);
        textNewFoodOmega3 = findViewById(R.id.text_addnew_omega3);
        textNewFoodOmega6 = findViewById(R.id.text_addnew_omega6);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!validate(textNewFoodName) | !validate(textNewFoodPortionSize) | !validate(textNewFoodEnergy) | !validate(textNewFoodProteins)
                        | !validate(textNewFoodFats) | !validate(textNewFoodCarbohydrates) | !validate(textNewFoodFibers) | !validate(textNewFoodOmega3) |!validate(textNewFoodOmega6)){
                    return;
                }

                DatabaseReference new_food_database = firebaseDatabase.getReference("Food");



                String foodIDs = foodName.getText().toString();
                String foodNames = foodName.getText().toString();
                String portionSizes = portionSize.getText().toString();
                String energies = energy.getText().toString();
                String proteins = protein.getText().toString();
                String fats = fat.getText().toString();
                String carbohydrates = carbohydrate.getText().toString();
                String fibers = fiber.getText().toString();
                String omega3s = omega3.getText().toString();
                String omega6s = omega6.getText().toString();

                Food foodInformation = new Food(foodNames ,portionSizes, energies, proteins, fats, carbohydrates, fibers, omega3s, omega6s);

                new_food_database.child(foodIDs).setValue(foodInformation).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(AddNewFoodActivity.this, "New Food Created", Toast.LENGTH_LONG).show();

                            startActivity(new Intent(AddNewFoodActivity.this, ProfileActivity.class));
                        }else{
                            Toast.makeText(AddNewFoodActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddNewFoodActivity.this, ProfileActivity.class));
            }
        });
    }

    private boolean validate (TextInputLayout inputLayout){
        String input = Objects.requireNonNull(inputLayout.getEditText()).getText().toString().trim();

        if(input.isEmpty()){
            inputLayout.setError("Field can't be empty");
            return false;
        }else{
            inputLayout.setError(null);
            return true;
        }
    }
}
