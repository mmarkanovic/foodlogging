package com.example.foodlogging2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Objects;


public class ProfileActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
   // private ActionBarDrawerToggle barDrawerToggle;

    TextView userEmail;
    RecyclerView recyclerViewBreakfast;
    RecyclerView recyclerViewLunch;
    RecyclerView recyclerViewSnack;
    RecyclerView recyclerViewDinner;

    

    ArrayList<Food> foodInformationBreakfast;
    ArrayList<Food> foodInformationLunch;
    ArrayList<Food> foodInformationSnacks;
    ArrayList<Food> foodInformationDinner;


    IntakeRecyclerAdapterBreakfast breakfastAdapter;
    IntakeRecyclerAdapterLunch lunchAdapter;
    IntakeRecyclerAdapterSnack snackAdapter;
    IntakeRecyclerAdapterDinner dinnerAdapter;


    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;
    DatabaseReference databaseDailyFoodReferenceBreakfast;
    DatabaseReference databaseDailyFoodReferenceLunch;
    DatabaseReference databaseDailyFoodReferenceSnack;
    DatabaseReference databaseDailyFoodReferenceDinner;
    DatabaseReference databaseDailySummary;
    String userID;

    TextView firebaseFats;
    TextView firebaseOmega3s;
    TextView firebaseOmega6s;
    Button addFood, addToSummary;
    //TextView textViewDate;


    //Current date
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date GetDate = new Date();
    String currentDate = timeStampFormat.format(GetDate);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        //Firebase stuff
        //-------------------------------------------------------------
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        firebaseFats = findViewById(R.id.tvfbFats);
        firebaseOmega3s = findViewById(R.id.tvProfileActivityOmega3);
        firebaseOmega6s = findViewById(R.id.tvProfileActivityOmega6);
        addFood = findViewById(R.id.btnGoToAllFood);
        addToSummary= findViewById(R.id.btnAddToSummary);
        userID = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);

        databaseDailySummary=databaseReference.child("userData");

        databaseDailyFoodReferenceBreakfast = databaseReference.child(currentDate).child("Doručak");
        databaseDailyFoodReferenceLunch = databaseReference.child(currentDate).child("Ručak");
        databaseDailyFoodReferenceSnack = databaseReference.child(currentDate).child("Užina");
        databaseDailyFoodReferenceDinner = databaseReference.child(currentDate).child("Večera");

        //-------------------------------------------------------------
        //Navigation drawer
        // ------------------------------------------------------------
        drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

        userEmail = header.findViewById(R.id.tvUserEmail1);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (firebaseUser == null) {
            startActivity(new Intent(this, LoginActivity.class));
            navigationView.setCheckedItem(R.id.nav_home);
        }

        userEmail.setText(firebaseUser.getEmail());
        // ------------------------------------------------------------

        //Pulling data from the database
        // ------------------------------------------------------------

         final double[] sumBreakfast = {0}, sumLunch = {0}, sumSnacks = {0}, sumDinner = {0}, omega3sumBreakfast = {0},
        omega3sumLunch = {0}, omega3sumSnack = {0}, omega3sumDinner = {0}, omega6sumBreakfast = {0}, omega6sumLunch = {0},
                 omega6sumSnack = {0}, omega6sumDinner = {0};
         
        //RecyclerView - Breakfast
        // ------------------------------------------------------------
        recyclerViewBreakfast = findViewById(R.id.rvIntakeFoodBreakfast);
        recyclerViewBreakfast.setLayoutManager(new LinearLayoutManager(this));

        foodInformationBreakfast = new ArrayList<>();


        //Pulling data from the database - Breakfast
        // ------------------------------------------------------------
        databaseDailyFoodReferenceBreakfast.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()){

                    foodInformationBreakfast.add(ds.getValue(Food.class));


                    Map<String,Object> map = (Map<String,Object>) ds.getValue();

                    assert map != null;
                    Object fats = map.get("fat");
                    Object omega3 = map.get("omega3");
                    Object omega6 = map.get("omega6");

                    double fatValue = Double.parseDouble(String.valueOf(fats));
                    double omega3Value = Double.parseDouble(String.valueOf(omega3));
                    double omega6Value = Double.parseDouble(String.valueOf(omega6));

                    sumBreakfast[0] += fatValue;
                    omega3sumBreakfast[0] += omega3Value;
                    omega6sumBreakfast[0] += omega6Value;


                }
                breakfastAdapter = new IntakeRecyclerAdapterBreakfast(ProfileActivity.this, foodInformationBreakfast);
                recyclerViewBreakfast.setAdapter(breakfastAdapter);



            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ProfileActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


        if ((getIntent().hasExtra("deleted_food_breakfast"))){

            String deletedBreakfastFoodId = getIntent().getStringExtra("food_id_breakfast");
            DatabaseReference breakfastDeleted= FirebaseDatabase.getInstance().getReference().child("Users").child(userID)
                    .child(currentDate).child("Doručak").child(deletedBreakfastFoodId);
            breakfastDeleted.removeValue();

            startActivity(new Intent(ProfileActivity.this, ProfileActivity.class));
        }

        //RecyclerView - Lunch
        // ------------------------------------------------------------
        recyclerViewLunch = findViewById(R.id.rvIntakeFoodLunch);
        recyclerViewLunch.setLayoutManager(new LinearLayoutManager(this));

        foodInformationLunch = new ArrayList<>();

        //Pulling data from the database - Lunch
        // ------------------------------------------------------------
        databaseDailyFoodReferenceLunch.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()){

                    foodInformationLunch.add(ds.getValue(Food.class));

                    Map<String,Object> map = (Map<String,Object>) ds.getValue();

                    assert map != null;
                    Object fats = map.get("fat");
                    Object omega3 = map.get("omega3");
                    Object omega6 = map.get("omega6");

                    double fatValue = Double.parseDouble(String.valueOf(fats));
                    double omega3Value = Double.parseDouble(String.valueOf(omega3));
                    double omega6Value = Double.parseDouble(String.valueOf(omega6));

                    sumLunch[0] += fatValue;
                    omega3sumLunch[0] += omega3Value;
                    omega6sumLunch[0] += omega6Value;
                }
                lunchAdapter = new IntakeRecyclerAdapterLunch(ProfileActivity.this, foodInformationLunch);
                recyclerViewLunch.setAdapter(lunchAdapter);

                recyclerViewLunch.hasOnClickListeners();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ProfileActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        if (getIntent().hasExtra("deleted_food_lunch")){

            String deletedLunchFoodId = getIntent().getStringExtra("food_id_lunch");
            DatabaseReference lunchDeleted= FirebaseDatabase.getInstance().getReference().child("Users").child(userID)
                    .child(currentDate).child("Ručak").child(deletedLunchFoodId);

            lunchDeleted.removeValue();

            startActivity(new Intent(ProfileActivity.this, ProfileActivity.class));
        }

        //RecyclerView - Snack
        // ------------------------------------------------------------
        recyclerViewSnack = findViewById(R.id.rvIntakeFoodSnack);
        recyclerViewSnack.setLayoutManager(new LinearLayoutManager(this));

        foodInformationSnacks = new ArrayList<>();

        //Pulling data from the database - Snack
        // ------------------------------------------------------------
        databaseDailyFoodReferenceSnack.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){

                    foodInformationSnacks.add(ds.getValue(Food.class));

                    Map<String,Object> map = (Map<String,Object>) ds.getValue();

                    assert map != null;
                    Object fats = map.get("fat");
                    Object omega3 = map.get("omega3");
                    Object omega6 = map.get("omega6");

                    double fatValue = Double.parseDouble(String.valueOf(fats));
                    double omega3Value = Double.parseDouble(String.valueOf(omega3));
                    double omega6Value = Double.parseDouble(String.valueOf(omega6));

                    sumSnacks[0] += fatValue;
                    omega3sumSnack[0] += omega3Value;
                    omega6sumSnack[0] += omega6Value;
                }

                snackAdapter = new IntakeRecyclerAdapterSnack(ProfileActivity.this, foodInformationSnacks);
                recyclerViewSnack.setAdapter(snackAdapter);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ProfileActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        if (getIntent().hasExtra("deleted_food_snacks")){

            String deletedSnackFoodId = getIntent().getStringExtra("food_id_snack");

            DatabaseReference snackDeleted = FirebaseDatabase.getInstance().getReference().child("Users").child(userID)
                    .child(currentDate).child("Užina").child(deletedSnackFoodId);

            snackDeleted.removeValue();
            startActivity(new Intent(ProfileActivity.this, ProfileActivity.class));
        }

        //RecyclerView - Dinner
        // ------------------------------------------------------------
        recyclerViewDinner = findViewById(R.id.rvIntakeFoodDinner);
        recyclerViewDinner.setLayoutManager(new LinearLayoutManager(this));

        foodInformationDinner = new ArrayList<>();

        //Pulling data from the database - Dinner
        // ------------------------------------------------------------
        databaseDailyFoodReferenceDinner.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    foodInformationDinner.add(ds.getValue(Food.class));

                    Map<String, Object> map = (Map<String, Object>) ds.getValue();

                    assert map != null;
                    Object fats = map.get("fat");
                    Object omega3 = map.get("omega3");
                    Object omega6 = map.get("omega6");

                    double fatValue = Double.parseDouble(String.valueOf(fats));
                    double omega3Value = Double.parseDouble(String.valueOf(omega3));
                    double omega6Value = Double.parseDouble(String.valueOf(omega6));

                    sumDinner[0] += fatValue;
                    omega3sumDinner[0] += omega3Value;
                    omega6sumDinner[0] += omega6Value;
                }

                dinnerAdapter = new IntakeRecyclerAdapterDinner(ProfileActivity.this, foodInformationDinner);
                recyclerViewDinner.setAdapter(dinnerAdapter);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ProfileActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        if (getIntent().hasExtra("deleted_food_dinner")){
           String deletedDinnerFoodId = getIntent().getStringExtra("food_id_dinner");

            DatabaseReference dinnerDeleted= FirebaseDatabase.getInstance().getReference().child("Users").child(userID)
                    .child(currentDate).child("Večera").child(deletedDinnerFoodId);

            dinnerDeleted.removeValue();

            startActivity(new Intent(ProfileActivity.this, ProfileActivity.class));
        }

        //Calculating the daily fats
        // ------------------------------------------------------------
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //textViewDate.setText("Today");

                String user_fats = dataSnapshot.child("userData").child("fats").getValue(String.class);
                String user_omega3 = dataSnapshot.child("userData").child("omega3").getValue(String.class);
                String user_omega6 = dataSnapshot.child("userData").child("omega6").getValue(String.class);

                //String user_daily_fats = dataSnapshot.child(currentDate).child().child("Fats");

                assert user_fats != null;
                double user_fats_number = Double.parseDouble(user_fats);

                assert user_omega3 != null;
                double user_omega3_number = Double.parseDouble(user_omega3);

                assert user_omega6 != null;
                double user_omega6_number = Double.parseDouble(user_omega6);

               double fats_added_breakfast =  round(sumBreakfast[0],2);
               double omega3_added_breakfast = round(omega3sumBreakfast[0],2);
               double omega6_added_breakfast = round(omega6sumBreakfast[0], 2);

               double fats_added_lunch = round(sumLunch[0],2);
               double omega3_added_lunch = round(omega3sumLunch[0],2);
               double omega6_added_lunch = round(omega6sumLunch[0], 2);

               double fats_added_snack = round(sumSnacks[0], 2);
               double omega3_added_snack = round(omega3sumSnack[0],2);
               double omega6_added_snack = round(omega6sumSnack[0], 2);

               double fats_added_dinner = round(sumDinner[0],2);
               double omega3_added_dinner = round(omega3sumDinner[0],2);
               double omega6_added_dinner = round(omega6sumDinner[0], 2);

               double fats_sum = fats_added_breakfast + fats_added_lunch + fats_added_snack +fats_added_dinner;
               double omega3_sum = omega3_added_breakfast + omega3_added_lunch + omega3_added_snack + omega3_added_dinner;
               double omega6_sum = omega6_added_breakfast + omega6_added_lunch + omega6_added_snack + omega6_added_dinner;

               final double fats_sum_rounded = round(fats_sum, 2);
               final double omega3_sum_rounded = round(omega3_sum,2);
               final double omega6_sum_rounded = round(omega6_sum,2);

               double user_fats_minus_fat_sum = user_fats_number - fats_sum;
               double user_fats_minus_fat_sum_rounded = round(user_fats_minus_fat_sum, 2);

               double user_omega3_minus_omega3_sum = user_omega3_number - omega3_sum_rounded;
               double user_omega3_minus_omega3_sum_rounded = round(user_omega3_minus_omega3_sum, 2);

               double user_omega6_minus_omega6_sum = user_omega6_number - omega6_sum_rounded;
                double user_omega6_minus_omega6_sum_rounded = round(user_omega6_minus_omega6_sum, 2);

                String setTextFirebaseFats = user_fats + " g" + " - " + fats_sum_rounded + " g = " + (user_fats_minus_fat_sum_rounded)
                        + " g (preostalo Masti)";

                firebaseFats.setText(setTextFirebaseFats);

                if (fats_sum_rounded>user_fats_number){
                    firebaseFats.setTextColor(Color.RED);

                    String setTextFirebaseFatsRed =user_fats + " g" + " - " + fats_sum_rounded + " g = " + (user_fats_minus_fat_sum_rounded)
                            + " g (prekoračena dnevna granica)";

                    firebaseFats.setText(setTextFirebaseFatsRed);
                }

                String setTextFirebaseOmega3 = user_omega3_number + " g" + " - " + omega3_sum_rounded + " g = " + (user_omega3_minus_omega3_sum_rounded)
                        + " g (preostalo Omega3 Masti)";

                firebaseOmega3s.setText(setTextFirebaseOmega3);

                if (omega3_sum_rounded>user_omega3_number){
                    firebaseOmega3s.setTextColor(Color.RED);

                    String setTextFirebaseOmega3Red = user_omega3_number + " g" + " - " + omega3_sum_rounded + " g = " + (user_omega3_minus_omega3_sum_rounded)
                            + " g (prekoračena dnevna granica)";

                    firebaseOmega3s.setText(setTextFirebaseOmega3Red);
                }

                String setTextFirebaseOmega6 = user_omega6 + " g" + " - " + omega6_sum_rounded + " g = " + (user_omega6_minus_omega6_sum_rounded)
                        + " g (preostalo Omega6 Masti)";

                firebaseOmega6s.setText(setTextFirebaseOmega6);

                if (omega6_sum_rounded>user_omega6_number){
                    firebaseOmega6s.setTextColor(Color.RED);

                    String setTextFirebaseOmega6Red = user_omega6 + " g" + " - " + omega6_sum_rounded + " g = " + (user_omega6_minus_omega6_sum_rounded)
                            + " g (prekoračena dnevna granica)";

                    firebaseOmega6s.setText(setTextFirebaseOmega6Red);
                }


                final String omega3Sum = String.valueOf(omega3_sum_rounded);
                final String omega6Sum = String.valueOf(omega6_sum_rounded);

                final String dailyFats = String.valueOf(fats_sum_rounded);

                final Food dailySummaryFats = new Food (dailyFats, omega3Sum, omega6Sum);



                addFood.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(ProfileActivity.this, AllFoodActivity.class));
                    }
                });
                
                addToSummary.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        databaseDailySummary.child("dates").child(currentDate).setValue(dailySummaryFats).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @SuppressLint("IntentReset")
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(ProfileActivity.this, "Podatci su spremljeni", Toast.LENGTH_SHORT).show();

                                /*
                                Intent intentMail = new Intent(Intent.ACTION_SENDTO);
                                intentMail.setType("text/plain");
                                intentMail.setData(Uri.parse("mailto:"));

                                intentMail.putExtra(Intent.EXTRA_EMAIL, new String[]{});
                                intentMail.putExtra(Intent.EXTRA_SUBJECT, "Dnevni unos za dan " + currentDate);
                                intentMail.putExtra(Intent.EXTRA_TEXT, "Unešeno masti "+ dailyFats + " g. Unešeno Omega3 masti "+omega3Sum
                                        +" g. Unešeno Omega6 masti "+omega6Sum+" g");


                                intentMail.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                startActivity(intentMail);

                                 */
                               Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
                               startActivity(intent);
                            }
                        });
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ProfileActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        };

        databaseReference.addListenerForSingleValueEvent(valueEventListener);

    }

    //Navigation drawer
    // ------------------------------------------------------------
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.nav_home:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
            case R.id.nav_addFood:
                startActivity(new Intent(this, AddNewFoodActivity.class));
                break;
            case R.id.nav_allFood:
                startActivity(new Intent(this, AllFoodActivity.class));
                break;
            case R.id.nav_profile:
                startActivity(new Intent(this, AccountSummaryActivity.class));
                break;
            case R.id.nav_logout:
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this, MainActivity.class));
        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }






}
