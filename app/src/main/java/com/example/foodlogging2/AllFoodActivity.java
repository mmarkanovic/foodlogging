package com.example.foodlogging2;


import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class AllFoodActivity extends AppCompatActivity implements AllFoodAdapterClass.OnItemListener  {

    FirebaseAuth firebaseAuth;
    String user_id;
    DatabaseReference databaseReference, userIntakeReference;
    RecyclerView recyclerView;
    SearchView searchView;
    ArrayList<Food> foodInformation;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_allfood);

        firebaseAuth = FirebaseAuth.getInstance ();
        user_id= Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Food");
        userIntakeReference = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);

        recyclerView = findViewById(R.id.rvAllFood);
        searchView = findViewById(R.id.svAllFood);

        foodInformation = new ArrayList<>();

        final AllFoodAdapterClass allFoodAdapterClass = new AllFoodAdapterClass(foodInformation, this);

        if (databaseReference != null){
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()){

                        for (DataSnapshot ds : dataSnapshot.getChildren()){

                            foodInformation.add(ds.getValue(Food.class));
                        }
                        recyclerView.setAdapter(allFoodAdapterClass);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(AllFoodActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

        if (searchView != null){

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    search(s);
                    return false;
                }
            });
        }
    }

    private void search (String string){

        ArrayList<Food> foodList = new ArrayList<>();

        for (Food object : foodInformation){
            if (object.getFoodID().toLowerCase().contains(string.toLowerCase())){
                foodList.add(object);
            }
        }

        AllFoodAdapterClass allFoodAdapterClass = new AllFoodAdapterClass(foodList, this);
        recyclerView.setAdapter(allFoodAdapterClass);
    }

    @Override
    public void onItemClick(final int position) {

        Intent intent;
        intent = new Intent(AllFoodActivity.this, AddFoodToIntakeActivity.class);
        intent.putExtra("selected_food", (Parcelable) foodInformation.get(position));
        startActivity(intent);

        Toast.makeText(AllFoodActivity.this, "Item clicked", Toast.LENGTH_LONG ).show();

    }
}

