package com.example.foodlogging2;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;


public class AccountSummaryActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;

    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    DatabaseReference databaseReference, summaryReference, dailySummaryReferenceBreakfast, dailySummaryReferenceLunch, dailySummaryReferenceSnack, dailySummaryReferenceDinner;
    String userID;
    TextView fatsNumberView, updateDailyFatsNumber, omega3NumberView, updateOmega3DailyNumber, omega6NumberView, updateOmega6DailyNumber;
    TextView startDate, endDate;
    Button saveData;

    //Current date
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date GetDate = new Date();
    String currentDate = timeStampFormat.format(GetDate);


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_accountsummary);


        fatsNumberView = findViewById(R.id.tvUserFatsUserSummary);
        updateDailyFatsNumber = findViewById(R.id.tvUserFatsUserUpdate);

        omega3NumberView = findViewById(R.id.tvUserOmega3UserSummary);
        updateOmega3DailyNumber = findViewById(R.id.tvUserOmega3UserUpdate);

        omega6NumberView = findViewById(R.id.tvUserOmega6UserSummary);
        updateOmega6DailyNumber = findViewById(R.id.tvUserOmega6UserUpdate);

        startDate = findViewById(R.id.tvUserSummaryStartDate);
        endDate = findViewById(R.id.tvUserSummaryEndDate);

        saveData = findViewById(R.id.btnSaveDataRange);


        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        userID = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users")
                .child(userID).child("userData");

        summaryReference = databaseReference.child("dates");

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}
                , PackageManager.PERMISSION_GRANTED);

        updateDailyFatsNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountSummaryActivity.this, EditDailyFatsActivity.class));
            }
        });

        updateOmega3DailyNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountSummaryActivity.this, EditDailyOmega3Activity.class));
            }
        });

        updateOmega6DailyNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountSummaryActivity.this, EditDailyOmega6Activity.class));
            }
        });


        drawer = findViewById(R.id.drawer_layout_account_summary);

        NavigationView navigationView = findViewById(R.id.nav_view_account_summary);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String user_fats = dataSnapshot.child("fats").getValue(String.class);
                String user_omega3 = dataSnapshot.child("omega3").getValue(String.class);
                String user_omega6 = dataSnapshot.child("omega6").getValue(String.class);

                fatsNumberView.setText(user_fats + " g");
                omega3NumberView.setText(user_omega3 + " g");
                omega6NumberView.setText(user_omega6 + " g");

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(AccountSummaryActivity.this, databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        };

        databaseReference.addListenerForSingleValueEvent(eventListener);

        startDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AccountSummaryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                startDate.setText(year + "-" + (month + 1) + "-" + day);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AccountSummaryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                endDate.setText(year + "-" + (month + 1) + "-" + day);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });

        saveData.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                CountDownLatch done = new CountDownLatch(1);

                String startDateName = startDate.getText().toString();
                String endDateName = endDate.getText().toString();
                String fileName = "From: " + startDateName + " Until: " + endDateName;

                SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");

                FileInputStream fileInputStream = null;
                Date startDateNumber = null;
                try {
                    startDateNumber = timeStampFormat.parse(startDateName);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Date endDateNumber = null;
                try {
                    endDateNumber = timeStampFormat.parse(endDateName);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    Date currentDateNumber = timeStampFormat.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                LocalDate startLocalDate = startDateNumber.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDate endLocalDate = endDateNumber.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                final Document document = new Document();
                final Paragraph paragraph = new Paragraph();

                final String filePath = Environment.getExternalStorageDirectory().getPath()+"/"+fileName+".pdf";
                final Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN,12,Font.BOLD);

                try {
                    PdfWriter.getInstance(document, new FileOutputStream(filePath));

                    document.open();
                    document.add(new Paragraph(fileName));

                    for (LocalDate date = startLocalDate; date.isBefore(endLocalDate.plusDays(1)); date=date.plusDays(1)) {

                        LocalDate currentLocalDate = date;
                        final String currentLocalDateString = currentLocalDate.toString();

                        //Doručak od tog dana

                        dailySummaryReferenceBreakfast = FirebaseDatabase.getInstance().getReference().child("Users")
                                .child(userID).child(currentLocalDateString).child("Doručak");

                        dailySummaryReferenceBreakfast.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                //get all of the children at this level
                                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                                try {
                                    document.add(new Paragraph(currentLocalDateString, smallBold));
                                    document.add(new Paragraph("Dorucak"));
                                } catch (DocumentException e) {
                                    e.printStackTrace();
                                }

                                for (DataSnapshot child:children) {

                                    Map<String, Object> map = (Map<String, Object>) child.getValue();

                                    assert map != null;
                                    Object foodId = map.get("foodID");
                                    Object portionSize = map.get("portionSize");
                                    Object calories = map.get("energy");
                                    Object protein = map.get("protein");
                                    Object carbohydrates = map.get("carbohydrate");
                                    Object fibers = map.get("fiber");
                                    Object fats = map.get("fat");
                                    Object omega3 = map.get("omega3");
                                    Object omega6 = map.get("omega6");

                                    assert foodId != null;
                                    String foodIdValue = foodId.toString();

                                    assert portionSize != null;
                                    String portionSizeValue = portionSize.toString();

                                    assert calories != null;
                                    String caloriesValue = calories.toString();

                                    assert protein != null;
                                    String proteinValue = protein.toString();

                                    assert carbohydrates != null;
                                    String carbohydratesValue = carbohydrates.toString();

                                    assert fibers != null;
                                    String fibersValue = fibers.toString();

                                    assert fats != null;
                                    String fatsValue= fats.toString();

                                    assert omega3 != null;
                                    String omega3Value = omega3.toString();

                                    assert omega6 != null;
                                    String omega6Value = omega6.toString();

                                    try {
                                        document.add(new Paragraph("\n"));
                                        PdfPTable table = new PdfPTable(1);

                                        PdfPCell cellFoodId = new PdfPCell(Phrase.getInstance("Ime hrane : " + foodIdValue));
                                        table.addCell(cellFoodId);

                                        PdfPCell cellPortionSize = new PdfPCell(Phrase.getInstance("Velicina porcije : " + portionSizeValue + " g"));
                                        table.addCell(cellPortionSize);

                                        PdfPCell cellCalories = new PdfPCell(Phrase.getInstance("Kalorije : " + caloriesValue + " g"));
                                        table.addCell(cellCalories);

                                        PdfPCell cellProtein = new PdfPCell(Phrase.getInstance("Proteini : " + proteinValue + " g"));
                                        table.addCell(cellProtein);

                                        PdfPCell cellCarbohydrates = new PdfPCell(Phrase.getInstance("Ugljikohidrati : " + carbohydratesValue + " g"));
                                        table.addCell(cellCarbohydrates);

                                        PdfPCell cellFibers = new PdfPCell(Phrase.getInstance("Vlakna : " + fibersValue + " g"));
                                        table.addCell(cellFibers);

                                        PdfPCell cellFats = new PdfPCell(Phrase.getInstance("Masti : " + fatsValue + " g"));
                                        table.addCell(cellFats);

                                        PdfPCell cellOmega3 = new PdfPCell(Phrase.getInstance("Omega 3 masti : " + omega3Value + " g"));
                                        table.addCell(cellOmega3);

                                        PdfPCell cellOmega6 = new PdfPCell(Phrase.getInstance("Omega 6 masti : " + omega6Value + " g"));
                                        table.addCell(cellOmega6);

                                        document.add(table);

                                        //document.add(new Paragraph(String.valueOf(child)));
                                    } catch (DocumentException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        //Ručak od tog dana

                        dailySummaryReferenceLunch = FirebaseDatabase.getInstance().getReference().child("Users")
                                .child(userID).child(currentLocalDateString).child("Ručak");

                        dailySummaryReferenceLunch.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                                try {
                                    document.add(new Paragraph(currentLocalDateString, smallBold));
                                    document.add(new Paragraph("Rucak"));
                                } catch (DocumentException e) {
                                    e.printStackTrace();
                                }

                                for (DataSnapshot child:children) {
                                    Map<String, Object> map = (Map<String, Object>) child.getValue();

                                    assert map != null;
                                    Object foodId = map.get("foodID");
                                    Object portionSize = map.get("portionSize");
                                    Object calories = map.get("energy");
                                    Object protein = map.get("protein");
                                    Object carbohydrates = map.get("carbohydrate");
                                    Object fibers = map.get("fiber");
                                    Object fats = map.get("fat");
                                    Object omega3 = map.get("omega3");
                                    Object omega6 = map.get("omega6");

                                    assert foodId != null;
                                    String foodIdValue = foodId.toString();

                                    assert portionSize != null;
                                    String portionSizeValue = portionSize.toString();

                                    assert calories != null;
                                    String caloriesValue = calories.toString();

                                    assert protein != null;
                                    String proteinValue = protein.toString();

                                    assert carbohydrates != null;
                                    String carbohydratesValue = carbohydrates.toString();

                                    assert fibers != null;
                                    String fibersValue = fibers.toString();

                                    assert fats != null;
                                    String fatsValue= fats.toString();

                                    assert omega3 != null;
                                    String omega3Value = omega3.toString();

                                    assert omega6 != null;
                                    String omega6Value = omega6.toString();

                                    try {

                                        document.add(new Paragraph("\n"));

                                        PdfPTable table = new PdfPTable(1);

                                        PdfPCell cellFoodId = new PdfPCell(Phrase.getInstance("Ime hrane : " + foodIdValue));
                                        table.addCell(cellFoodId);

                                        PdfPCell cellPortionSize = new PdfPCell(Phrase.getInstance("Velicina porcije : " + portionSizeValue + " g"));
                                        table.addCell(cellPortionSize);

                                        PdfPCell cellCalories = new PdfPCell(Phrase.getInstance("Kalorije : " + caloriesValue + " g"));
                                        table.addCell(cellCalories);

                                        PdfPCell cellProtein = new PdfPCell(Phrase.getInstance("Proteini : " + proteinValue + " g"));
                                        table.addCell(cellProtein);

                                        PdfPCell cellCarbohydrates = new PdfPCell(Phrase.getInstance("Ugljikohidrati : " + carbohydratesValue + " g"));
                                        table.addCell(cellCarbohydrates);

                                        PdfPCell cellFibers = new PdfPCell(Phrase.getInstance("Vlakna : " + fibersValue + " g"));
                                        table.addCell(cellFibers);

                                        PdfPCell cellFats = new PdfPCell(Phrase.getInstance("Masti : " + fatsValue + " g"));
                                        table.addCell(cellFats);

                                        PdfPCell cellOmega3 = new PdfPCell(Phrase.getInstance("Omega 3 masti : " + omega3Value + " g"));
                                        table.addCell(cellOmega3);

                                        PdfPCell cellOmega6 = new PdfPCell(Phrase.getInstance("Omega 6 masti : " + omega6Value + " g"));
                                        table.addCell(cellOmega6);

                                        document.add(table);

                                        document.add(new Paragraph("\n"));
                                    } catch (DocumentException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        //Užina od tog dana

                        dailySummaryReferenceSnack = FirebaseDatabase.getInstance().getReference().child("Users")
                                .child(userID).child(currentLocalDateString).child("Užina");

                        dailySummaryReferenceSnack.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                                try {
                                    document.add(new Paragraph(currentLocalDateString, smallBold));
                                    document.add(new Paragraph("Uzina"));
                                } catch (DocumentException e) {
                                    e.printStackTrace();
                                }

                                for (DataSnapshot child:children) {
                                    Map<String, Object> map = (Map<String, Object>) child.getValue();

                                    assert map != null;
                                    Object foodId = map.get("foodID");
                                    Object portionSize = map.get("portionSize");
                                    Object calories = map.get("energy");
                                    Object protein = map.get("protein");
                                    Object carbohydrates = map.get("carbohydrate");
                                    Object fibers = map.get("fiber");
                                    Object fats = map.get("fat");
                                    Object omega3 = map.get("omega3");
                                    Object omega6 = map.get("omega6");

                                    assert foodId != null;
                                    String foodIdValue = foodId.toString();

                                    assert portionSize != null;
                                    String portionSizeValue = portionSize.toString();

                                    assert calories != null;
                                    String caloriesValue = calories.toString();

                                    assert protein != null;
                                    String proteinValue = protein.toString();

                                    assert carbohydrates != null;
                                    String carbohydratesValue = carbohydrates.toString();

                                    assert fibers != null;
                                    String fibersValue = fibers.toString();

                                    assert fats != null;
                                    String fatsValue= fats.toString();

                                    assert omega3 != null;
                                    String omega3Value = omega3.toString();

                                    assert omega6 != null;
                                    String omega6Value = omega6.toString();

                                    try {

                                        document.add(new Paragraph("\n"));
                                        PdfPTable table = new PdfPTable(1);

                                        PdfPCell cellFoodId = new PdfPCell(Phrase.getInstance("Ime hrane : " + foodIdValue));
                                        table.addCell(cellFoodId);

                                        PdfPCell cellPortionSize = new PdfPCell(Phrase.getInstance("Velicina porcije : " + portionSizeValue + " g"));
                                        table.addCell(cellPortionSize);

                                        PdfPCell cellCalories = new PdfPCell(Phrase.getInstance("Kalorije : " + caloriesValue + " g"));
                                        table.addCell(cellCalories);

                                        PdfPCell cellProtein = new PdfPCell(Phrase.getInstance("Proteini : " + proteinValue + " g"));
                                        table.addCell(cellProtein);

                                        PdfPCell cellCarbohydrates = new PdfPCell(Phrase.getInstance("Ugljikohidrati : " + carbohydratesValue + " g"));
                                        table.addCell(cellCarbohydrates);

                                        PdfPCell cellFibers = new PdfPCell(Phrase.getInstance("Vlakna : " + fibersValue + " g"));
                                        table.addCell(cellFibers);

                                        PdfPCell cellFats = new PdfPCell(Phrase.getInstance("Masti : " + fatsValue + " g"));
                                        table.addCell(cellFats);

                                        PdfPCell cellOmega3 = new PdfPCell(Phrase.getInstance("Omega 3 masti : " + omega3Value + " g"));
                                        table.addCell(cellOmega3);

                                        PdfPCell cellOmega6 = new PdfPCell(Phrase.getInstance("Omega 6 masti : " + omega6Value + " g"));
                                        table.addCell(cellOmega6);

                                        document.add(table);

                                        document.add(new Paragraph("\n"));
                                    } catch (DocumentException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        //Večera od tog dana

                        dailySummaryReferenceDinner = FirebaseDatabase.getInstance().getReference().child("Users")
                                .child(userID).child(currentLocalDateString).child("Večera");

                        dailySummaryReferenceDinner.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                                try {
                                    document.add(new Paragraph(currentLocalDateString, smallBold));
                                    document.add(new Paragraph("Vecera"));
                                } catch (DocumentException e) {
                                    e.printStackTrace();
                                }

                                for (DataSnapshot child:children) {
                                    Map<String, Object> map = (Map<String, Object>) child.getValue();

                                    assert map != null;
                                    Object foodId = map.get("foodID");
                                    Object portionSize = map.get("portionSize");
                                    Object calories = map.get("energy");
                                    Object protein = map.get("protein");
                                    Object carbohydrates = map.get("carbohydrate");
                                    Object fibers = map.get("fiber");
                                    Object fats = map.get("fat");
                                    Object omega3 = map.get("omega3");
                                    Object omega6 = map.get("omega6");

                                    assert foodId != null;
                                    String foodIdValue = foodId.toString();

                                    assert portionSize != null;
                                    String portionSizeValue = portionSize.toString();

                                    assert calories != null;
                                    String caloriesValue = calories.toString();

                                    assert protein != null;
                                    String proteinValue = protein.toString();

                                    assert carbohydrates != null;
                                    String carbohydratesValue = carbohydrates.toString();

                                    assert fibers != null;
                                    String fibersValue = fibers.toString();

                                    assert fats != null;
                                    String fatsValue= fats.toString();

                                    assert omega3 != null;
                                    String omega3Value = omega3.toString();

                                    assert omega6 != null;
                                    String omega6Value = omega6.toString();

                                    try {

                                        document.add(new Paragraph("\n"));
                                        PdfPTable table = new PdfPTable(1);

                                        PdfPCell cellFoodId = new PdfPCell(Phrase.getInstance("Ime hrane : " + foodIdValue));
                                        table.addCell(cellFoodId);

                                        PdfPCell cellPortionSize = new PdfPCell(Phrase.getInstance("Velicina porcije : " + portionSizeValue + " g"));
                                        table.addCell(cellPortionSize);

                                        PdfPCell cellCalories = new PdfPCell(Phrase.getInstance("Kalorije : " + caloriesValue + " g"));
                                        table.addCell(cellCalories);

                                        PdfPCell cellProtein = new PdfPCell(Phrase.getInstance("Proteini : " + proteinValue + " g"));
                                        table.addCell(cellProtein);

                                        PdfPCell cellCarbohydrates = new PdfPCell(Phrase.getInstance("Ugljikohidrati : " + carbohydratesValue + " g"));
                                        table.addCell(cellCarbohydrates);

                                        PdfPCell cellFibers = new PdfPCell(Phrase.getInstance("Vlakna : " + fibersValue + " g"));
                                        table.addCell(cellFibers);

                                        PdfPCell cellFats = new PdfPCell(Phrase.getInstance("Masti : " + fatsValue + " g"));
                                        table.addCell(cellFats);

                                        PdfPCell cellOmega3 = new PdfPCell(Phrase.getInstance("Omega 3 masti : " + omega3Value + " g"));
                                        table.addCell(cellOmega3);

                                        PdfPCell cellOmega6 = new PdfPCell(Phrase.getInstance("Omega 6 masti : " + omega6Value + " g"));
                                        table.addCell(cellOmega6);

                                        document.add(table);

                                        document.add(new Paragraph("\n"));
                                    } catch (DocumentException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        //document.newPage();
                        // Toast.makeText(this, "Saved to " + getFilesDir() + "/" +fileName, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e){
                    Toast.makeText(AccountSummaryActivity.this, "This is Error msg :" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(AccountSummaryActivity.this, "Datoteka spremljena u Unutarnju memoriju", Toast.LENGTH_SHORT).show();
                        document.close();
                    }
                }, 4000);
            }

        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
            case R.id.nav_addFood:
                startActivity(new Intent(this, AddNewFoodActivity.class));
                break;
            case R.id.nav_allFood:
                startActivity(new Intent(this, AllFoodActivity.class));
                break;
            case R.id.nav_profile:
                startActivity(new Intent(this, AccountSummaryActivity.class));
                break;
            case R.id.nav_logout:
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this, MainActivity.class));
        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}












