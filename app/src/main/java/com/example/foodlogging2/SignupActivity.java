package com.example.foodlogging2;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.textservice.SuggestionsInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SignupActivity extends AppCompatActivity {


    EditText email;
    EditText password;
    EditText fatGramsReg;
    EditText fatGramsRegOmega3;
    EditText fatGramsRegOmega6;
    Button signup;
    TextView textView;
    TextInputLayout textSignupEmail;
    TextInputLayout textSignupPassword;
    TextInputLayout textSignupFats;
    TextInputLayout textSignupOmega3;
    TextInputLayout textSignupOmega6;


    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

       email = findViewById(R.id.etEmail);
       password = findViewById(R.id.etPass);
       fatGramsReg = findViewById(R.id.etFats);
       fatGramsRegOmega3 = findViewById(R.id.etOmega3);
       fatGramsRegOmega6 = findViewById(R.id.etOmega6);
       signup = findViewById(R.id.btnSignup);
       textView = findViewById(R.id.linkLogin);


       textSignupEmail = findViewById(R.id.text_signup_email);
       textSignupPassword = findViewById(R.id.text_signup_password);
       textSignupFats = findViewById(R.id.text_signup_fats);
       textSignupOmega3 = findViewById(R.id.text_signup_omega3);
       textSignupOmega6 = findViewById(R.id.text_signup_omega6);


       firebaseAuth = FirebaseAuth.getInstance();
       firebaseDatabase = FirebaseDatabase.getInstance();




       signup.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {


               if(!validate(textSignupPassword) | !validate(textSignupFats) | !validateEmail()
                       | !validate(textSignupOmega3) | !validate(textSignupOmega6)){
                   return;
               }


               final String fatGramsRegistration = fatGramsReg.getText().toString().trim();
               final String omega3GramsRegistration = fatGramsRegOmega3.getText().toString().trim();
               final String omega6GramsRegistration = fatGramsRegOmega6.getText().toString().trim();

               firebaseAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                   @Override
                   public void onComplete(@NonNull Task<AuthResult> task) {
                       if (task.isSuccessful()){

                           String user_id = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

                           DatabaseReference current_user_database = firebaseDatabase.getReference("Users");


                               User userInformation = new User(fatGramsRegistration, omega3GramsRegistration, omega6GramsRegistration);

                               current_user_database.child(user_id).child("userData").setValue(userInformation).addOnCompleteListener(new OnCompleteListener<Void>() {
                                   @Override
                                   public void onComplete(@NonNull Task<Void> registrationTask) {
                                       if (registrationTask.isSuccessful()){

                                           firebaseAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                               @Override
                                               public void onComplete(@NonNull Task<Void> task) {

                                                   if (task.isSuccessful()){
                                                       Toast.makeText(SignupActivity.this, "Usjepšno ste se registrirali. " +
                                                               "Provjerite email za verifikaciju", Toast.LENGTH_LONG).show();

                                                       Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                                                       intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                       startActivity(intent);
                                                   }else{
                                                       Toast.makeText(SignupActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_LONG).show();
                                                   }


                                               }
                                           });

                                       }else{

                                           Toast.makeText(SignupActivity.this, Objects.requireNonNull(registrationTask.getException()).getMessage(), Toast.LENGTH_LONG).show();
                                       }
                                   }
                               });


                       }
                    }
               });
           }
       });

       textView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
               intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               startActivity(intent);
           }
       });

    }

    private boolean validate (TextInputLayout inputLayout){
        String input = Objects.requireNonNull(inputLayout.getEditText()).getText().toString().trim();

        if(input.isEmpty()){
            inputLayout.setError("Polje ne može biti prazno");
            return false;
        }else{
            inputLayout.setError(null);
            return true;
        }
    }

    private boolean validateEmail(){
        String emailInput = Objects.requireNonNull(textSignupEmail.getEditText()).getText().toString().trim();

        if (emailInput.isEmpty()){
            textSignupEmail.setError("Polje ne može biti prazno");
            return false;
        }else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()){
            textSignupEmail.setError("Unesite ispravnu email adresu");
            return false;
        }else{
            textSignupEmail.setError(null);
            return true;
        }
    }
}
