package com.example.foodlogging2;

public class User {

    public String fats;
    public String date;
    public String omega3;
    public String omega6;


    public String getFats() {
        return fats;
    }

    public void setFats(String fats) {
        this.fats = fats;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOmega3(){return omega3;}

    public void setOmega3(String omega3s){this.omega3=omega3s;}

    public String getOmega6(){return omega6;}

    public void setOmega6(String omega6s){this.omega6= omega6s;}


    public User(String fatGramsReg, String omega3s, String omega6s) {

        this.fats = fatGramsReg;
        this.omega3 = omega3s;
        this.omega6 = omega6s;
    }

    public void DateAndUser(String fatDaily){
        this.fats = fatDaily;
    }
}
